#!/bin/bash

## This script will replace the snapshot used to set up
## a local copy with your current (sanitized) database.
## Run the script from the project root folder, like this:
## $ database/generate-snapshot.sh

# Copy the current one, to bring it back later.
ddev export-db --file=./database/original.sql.gz

# Sanitize the DB and export it.
ddev drush -y sql:sanitize
ddev export-db --file=./database/snapshot.sql.gz

# Restore the previous database.
ddev import-db --file=./database/original.sql.gz
rm ./database/original.sql.gz

echo "*********************************************************************************"
echo "* Remember to run 'ddev drush cex' so the 'config' folder matches the database. *"
echo "*********************************************************************************"
