<?php

$config['system.logging']['error_level'] = 'verbose';

$databases['default']['default'] = [
  'database' => getenv('DB_NAME'),
  'username' => getenv('DB_USERNAME'),
  'password' => getenv('DB_PASSWORD'),
  'host' => getenv('DB_HOST'),
  'port' => '3306',
  'driver' => 'mysql',
  'charset' => 'utf8mb4',
  'collation' => 'utf8mb4_general_ci',
  'prefix' => '',
];

$settings['hash_salt'] = getenv('DRUPAL_HASH_SALT') ?? 'yqtH0UsW2Zg_bh_qb-gnq-653YvO1qUycLkN1YsSU6Di43Lt0AuPuK62uNAr3yuQ82pjjHdD5g';
$settings['config_sync_directory'] = '../config/default';

$settings['config_exclude_modules'] = [
  'devel_generate',
  'stage_file_proxy',
  'config_inspector',
  'devel',
  'migrate_devel',
  'upgrade_status',
  'upgrade_rector',
];

$config['stage_file_proxy.settings']['origin'] = 'https://www.drupal.es';

$config['google_analytics.settings']['account'] = 'UA-XXXXXXX-XX';

$config['gin.settings']['preset_accent_color'] = 'custom';

// #58b058 - (Default) Green for local.
$config['gin.settings']['accent_color'] = '#58b058';

// $settings['config_readonly'] = FALSE;
