<?php

$config['system.logging']['error_level'] = 'hide';

// This will prevent Drupal from setting read-only permissions on sites/default.
$settings['skip_permissions_hardening'] = TRUE;

// This will ensure the site can only be accessed through the intended host
// names. Additional host patterns can be added for custom configurations.
$settings['trusted_host_patterns'] = ['.*'];

// Don't use Symfony's APCLoader. ddev includes APCu; Composer's APCu loader has
// better performance.
$settings['class_loader_auto_detect'] = FALSE;

// We are in prod, no need for stage_file_proxy.
$config['stage_file_proxy.settings']['origin'] = FALSE;

// @todo change the below with prod settings.
$config['google_analytics.settings']['account'] = 'UA-XXXXXXX-XX';

// #c73030 - Red red for prod.
$config['gin.settings']['accent_color'] = '#c73030';

// Do not allow config changes on prod via UI.
$settings['config_readonly'] = TRUE;
