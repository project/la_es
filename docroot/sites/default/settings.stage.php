<?php

// #c5c730 - Yellow for stage.
$config['gin.settings']['accent_color'] = '#c5c730';

// Do not allow config changes on stage via UI.
$settings['config_readonly'] = TRUE;
