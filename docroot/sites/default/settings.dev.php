<?php

// #5868b0 - Blue for dev.
$config['gin.settings']['accent_color'] = '#5868b0';

// Do not allow config changes on dev via UI.
if (PHP_SAPI !== 'cli') {
  $settings['config_readonly'] = TRUE;
}
