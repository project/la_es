# Asociación Española de Drupal

This is the code that powers the [drupal.es](https://www.drupal.es) site.

Thanks to the authors/contributors of the original clone of this project.
See them [here](la_eu/authors.md), gracias!

Useful links when working on the project:

- [Setup](setup.md)
- [Theme](theme.md)
